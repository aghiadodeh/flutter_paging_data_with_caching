class Env {
  static const String paginationApi = "https://nodejs-i4ts.onrender.com/api";
  static const int perPage = 10;
}
