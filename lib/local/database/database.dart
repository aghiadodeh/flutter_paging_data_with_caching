import 'package:flutter_paging_data_with_caching/local/dao/product_dao.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:floor/floor.dart';
import 'package:flutter_paging_data_with_caching/models/data/product.dart';
import '../converters/converter.dart';
import 'dart:async';

part 'database.g.dart'; // the generated code will be there

@TypeConverters([ProductConverter, ListConverter])
@Database(version: 1, entities: [Product])
abstract class AppDatabase extends FloorDatabase {
  ProductDao get productDao;
}
