import 'dart:convert';
import 'package:floor/floor.dart';
import 'package:flutter_paging_data_with_caching/models/data/product.dart';

class ProductConverter extends TypeConverter<Product, String> {
  @override
  Product decode(String databaseValue) {
    final map = json.decode(databaseValue);
    return Product.fromJson(map);
  }

  @override
  String encode(Product value) {
    return json.encode(value);
  }
}

class ListConverter extends TypeConverter<List<String>?, String?> {
  @override
  List<String>? decode(String? databaseValue) {
    return databaseValue?.split(";");
  }

  @override
  String? encode(List<String>? value) {
    return value?.join(";");
  }
}
