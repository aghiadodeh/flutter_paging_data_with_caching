import 'package:floor/floor.dart';
import 'package:flutter_paging_data_with_caching/config/env.dart';
import '../../models/data/product.dart';

@dao
abstract class ProductDao {
  @Query('SELECT * FROM Product LIMIT :perPage OFFSET :offset')
  Future<List<Product>> findProducts(int offset, int perPage);

  @Query('SELECT * FROM Product WHERE id = :id')
  Future<Product?> findProductById(String id);

  @Query('DELETE FROM Product WHERE id IN (SELECT id FROM Product LIMIT :perPage OFFSET :offset)')
  Future<int?> removeProductsByPage(int offset, int perPage);

  @Query('DELETE FROM Product')
  Future<List<Product>?> removeAllProducts();

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertProduct(Product product);

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertProducts(List<Product> products);

  @transaction
  Future<void> refreshProducts(List<Product> products, int page) async {
    final offset = (Env.perPage * page) - Env.perPage;
    await removeProductsByPage(offset, Env.perPage);
    await insertProducts(products);
  }
}
