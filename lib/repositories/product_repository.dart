import 'dart:async';
import 'dart:convert';
import 'package:flutter_paging_data_with_caching/models/data/product.dart';
import 'package:flutter_paging_data_with_caching/models/responses/base_response/base_response.dart';
import 'package:flutter_paging_data_with_caching/models/responses/list_response/list_response.dart';

import '../config/env.dart';
import 'package:http/http.dart' as http;

class ProductRepository {
  Future<ListResponse<Product>?> getProductsList(int page) async {
    /// fetch data from server
    final api = "${Env.paginationApi}/shop/products?page=$page&limit=${Env.perPage}";
    final http.Response response = await http.get(Uri.parse(api));
    final responseData = BaseResponse<ListResponse<Product>>.fromJson(
      jsonDecode(response.body),
      (json) => ListResponse.fromJson(
        json as Map<String, dynamic>,
        (p) => Product.fromJson(p as Map<String, dynamic>),
      ),
    );
    return responseData.data;
  }
}
