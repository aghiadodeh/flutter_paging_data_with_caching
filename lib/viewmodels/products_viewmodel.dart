import 'dart:async';

import 'package:animated_infinite_scroll_pagination/animated_infinite_scroll_pagination.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paging_data_with_caching/config/env.dart';
import 'package:flutter_paging_data_with_caching/local/dao/product_dao.dart';
import 'package:flutter_paging_data_with_caching/models/data/product.dart';
import 'package:flutter_paging_data_with_caching/repositories/product_repository.dart';
import 'package:get_it/get_it.dart';

import '../local/database/database.dart';

class ProductsViewModel with PaginationViewModel<Product> {
  final repository = ProductRepository();
  final _controller = StreamController<PaginationState<List<Product>>>();
  final _appDatabase = GetIt.I.get<AppDatabase>();
  ProductDao get productDao => _appDatabase.productDao;

  Stream<PaginationState<List<Product>>> get result async* {
    yield* _controller.stream;
  }

  /// decide whether two object represent the same Item
  @override
  bool areItemsTheSame(Product a, Product b) => a.id == b.id;

  /// fetch data from repository and emit by Stream to pagination-list
  ///
  /// set total items count -> stop loading on last page
  @override
  Future<void> fetchData(int page) async {
    /// emit loading
    _controller.add(const PaginationLoading());

    try {
      /// get cached data
      final offset = (Env.perPage * page) - Env.perPage;
      final cachedData = await productDao.findProducts(offset, Env.perPage);
      if (cachedData.isNotEmpty == true) {
        _controller.add(PaginationSuccess(cachedData, cached: true));
      }
      debugPrint("cached Data: ${cachedData.map((e) => e.toJson()).join("\n")}");

      /// fetch data from server
      final response = await repository.getProductsList(page);
      debugPrint("fetched Data: ${response?.data?.map((e) => e.toJson()).join("\n")}  ");

      /// emit fetched data
      final products = response?.data ?? [];
      _controller.add(PaginationSuccess(products));

      /// insert data to SQLite database
      productDao.refreshProducts(products, page);

      /// tell the view-model the total of items.
      /// this will stop loading more data when last data-chunk is loaded
      setTotal(response?.total ?? 0);
    } catch (error) {
      debugPrint(error.toString());

      /// emit error
      _controller.add(const PaginationError());
      setTotal(0);
    }
  }

  /// subscribe for list changes
  @override
  Stream<PaginationState<List<Product>>> streamSubscription() => result;
}
