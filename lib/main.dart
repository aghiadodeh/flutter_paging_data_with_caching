import 'package:flutter/material.dart';
import 'package:flutter_paging_data_with_caching/local/database/database.dart';
import 'package:get_it/get_it.dart';
import 'ui/main_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  GetIt.I.registerSingleton<AppDatabase>(await $FloorAppDatabase.databaseBuilder('local_app_db.db').build());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Pagination Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      debugShowCheckedModeBanner: false,
      home: const MainScreen(),
    );
  }
}
