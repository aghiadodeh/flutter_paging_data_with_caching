import 'package:flutter/material.dart';
import 'widgets/products_list.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SafeArea(
        child: ProductListWidget(),
      ),
    );
  }
}
