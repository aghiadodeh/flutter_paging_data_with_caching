import 'package:flutter/material.dart';
import 'package:flutter_paging_data_with_caching/models/data/product.dart';

class ProductWidget extends StatelessWidget {
  final Product product;
  const ProductWidget(this.product, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(color: Colors.grey.shade400, blurRadius: 2),
        ],
      ),
      child: Column(
        children: [
          if (product.images?.isNotEmpty == true)
            SizedBox(
              width: double.infinity,
              height: 65,
              child: Image.network(product.images!.first),
            ),
          const SizedBox(height: 8),
          Text(product.title ?? ''),
          Text(product.category ?? ''),
        ],
      ),
    );
  }
}
