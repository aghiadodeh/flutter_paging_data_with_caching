import 'package:animated_infinite_scroll_pagination/animated_infinite_scroll_pagination.dart';
import 'package:flutter/material.dart';
import 'package:flutter_paging_data_with_caching/models/data/product.dart';
import 'product_widget.dart';
import 'package:flutter_paging_data_with_caching/viewmodels/products_viewmodel.dart';

class ProductListWidget extends StatefulWidget {
  const ProductListWidget({Key? key}) : super(key: key);

  @override
  State<ProductListWidget> createState() => _ProductListWidgetState();
}

class _ProductListWidgetState extends State<ProductListWidget> {
  final viewModel = ProductsViewModel();

  @override
  void initState() {
    super.initState();
    viewModel
      ..listen() // observe data-list changes when repository update the list
      ..getPaginationList(); // fetch first chunk of data from server
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedInfiniteScrollView<Product>(
      viewModel: viewModel,
      itemBuilder: (index, item) => ProductWidget(item),
      refreshIndicator: true,
    );
  }
}
